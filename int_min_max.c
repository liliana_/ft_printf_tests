/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   int_min_max.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: liboerti <liboerti@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/11/24 13:45:50 by liboerti      #+#    #+#                 */
/*   Updated: 2021/11/24 13:50:23 by liboerti      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <limits.h>
#include <stdio.h>

void	ft_test_int_min_max(void)
{
	ft_printf("%x\n", INT_MAX);
	printf("%x\n", INT_MAX);
	ft_printf("%x\n", INT_MIN);
	printf("%x\n", INT_MIN);
	ft_printf("%X\n", INT_MAX);
	printf("%X\n", INT_MAX);
	ft_printf("%X\n", INT_MIN);
	printf("%X\n", INT_MIN);
	ft_printf("%d\n", INT_MAX);
	printf("%d\n", INT_MAX);
	ft_printf("%d\n", INT_MIN);
	printf("%d\n", INT_MIN);
	ft_printf("%i\n", INT_MAX);
	printf("%i\n", INT_MAX);
	ft_printf("%i\n", INT_MIN);
	printf("%i\n", INT_MIN);
	ft_printf("%u\n", INT_MAX);
	printf("%u\n", INT_MAX);
	ft_printf("%u\n", INT_MIN);
	printf("%u\n", INT_MIN);
}
