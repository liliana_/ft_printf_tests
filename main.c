/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: liboerti <liboerti@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/11/03 23:20:50 by liboerti      #+#    #+#                 */
/*   Updated: 2021/11/29 17:18:18 by liboerti      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"
#include "ft_defs.h"
#include <stdio.h>
#include <limits.h>
#include "test.h"
#include <string.h>

int	main(int argc, char **argv)
{
	void	*func;

	if (argc != 2)
		return (0);
	if (!strcmp(argv[1], "ft"))
		func = &ft_printf;
	else
		func = &printf;
	ft_test_regular_input(func);
	system("leaks a.out");
	return (0);
}
