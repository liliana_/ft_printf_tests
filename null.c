/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   null.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: liboerti <liboerti@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/11/24 13:51:47 by liboerti      #+#    #+#                 */
/*   Updated: 2021/11/29 15:32:51 by liboerti      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "ft_printf.h"
#include <stdio.h>

void	ft_test_null_input(PRINTF_PTR)
{
	ft_printf("%c", NULL);
	printf("%c", NULL);
	ft_printf("%s", NULL);
	printf("%s", NULL);
	ft_printf("%u", NULL);
	printf("%u", NULL);
	ft_printf("%i", NULL);
	printf("%i", NULL);
	ft_printf("%d", NULL);
	printf("%d", NULL);
	ft_printf("%p", NULL);
	printf("%p", NULL);
}
