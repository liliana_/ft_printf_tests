/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test.h                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: liboerti <liboerti@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/11/24 13:47:42 by liboerti      #+#    #+#                 */
/*   Updated: 2021/12/07 14:11:54 by liboerti      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
# define TEST_H
# define PRINTF_PTR int (*p)(const char *, ...)
# define T(f, ...) g_ret = p(f, __VA_ARGS__); fflush(stdout); ft_print_return(g_ret);
# define T_NC(f) g_ret = p(f); fflush(stdout); ft_print_return(g_ret);

void	ft_test_regular_input(PRINTF_PTR);

#endif
