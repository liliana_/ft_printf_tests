/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   regular_input.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: liboerti <liboerti@student.codam.nl>         +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/11/26 12:06:08 by liboerti      #+#    #+#                 */
/*   Updated: 2021/12/07 14:20:44 by liboerti      ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"
#include <stdio.h>
#include <stddef.h>
#include <limits.h>
#include "libft.h"
int	g_ret;

void	ft_print_return(int ret)
{
	ft_putstr_fd("\nreturn: ", 1);
	ft_putnbr_fd(ret, 1);
	ft_putchar_fd('\n', 1);
}

void	ft_test_str(PRINTF_PTR)
{
	T("%s", "test1");
	T("%s%s%s", "test1\n", "tes", "t2\n");
	T("%s%s%s", "test1\n", NULL, "t2\n");
}

void	ft_test_int(PRINTF_PTR)
{
	T("%d", INT_MIN);
	T("%d", INT_MAX);
	T("%d", 0);
	T("%d", 1000);
	T("%d", -1000);
	T("%d%d%d", 1000, 0, 15);
}

void	ft_test_no_conversion(PRINTF_PTR)
{
	T_NC("ashdkjahsdjkasdhhasjdkasdkjh\n\n\nasmndhaskdajs");
}

void	ft_test_uint(PRINTF_PTR)
{
	T("%u", INT_MIN);
	T("%u", INT_MAX);
	T("%u", UINT_MAX);
	T("%u", -1);
	T("%u", -5);
	T("%u", -10);
	T("%u", -1237);
	T("%u%u", 0, 0);
}

void	ft_test_combined(PRINTF_PTR)
{
	T("now it will print a 1:%sthis is printed by reference%s", "1", "2");
	T("%s, dsffkhjdfj, %p, %x, %X", "funny" , 16, 16, 16);
	T("%d%d%p%p%p%p%p", 42, 42, (void *)42, (void *)42, (void *)42, (void *)42, (void *)42);
	T("%d%p%%", 42, (void *)42);
	T("%d%p%%", 42, (void *)-1123);
}

void	ft_test_lhex(PRINTF_PTR)
{
	T("%X", INT_MIN);
	T("%X", INT_MAX);
	T("%X", UINT_MAX);
	T("%X", 15);
	T("%X", 16);
	T("%X", -1);
	T("%X", -2);
	T("%X", -3);
	T("%X", -4);
	T("%X", 0);
}

void	ft_test_ptr(PRINTF_PTR)
{
	T("%p", (void *)INT_MIN);
	T("%p", (void *) INT_MAX);
	T("%p", (void *) UINT_MAX);
	T("%p", (void *) NULL);
	T("%p", (void *) -1);
	T("%p", (void *) -2);
	T("%p", (void *) -3);
	T("%p", (void *) -4);
	T("%p", (void *) 0);
}

void	ft_test_uhex(PRINTF_PTR)
{
	T("%x", INT_MIN);
	T("%x", INT_MAX);
	T("%x", UINT_MAX);
	T("%x", 15);
	T("%x", 16);
	T("%x", -1);
	T("%x", -2);
	T("%x", -3);
	T("%x", -4);
	T("%x", 0);
}

void	ft_test_null_input(PRINTF_PTR)
{
	T("%c", NULL);
	T("%s", NULL);
	T("%u", NULL);
	T("%i", NULL);
	T("%d", NULL);
	T("%p", NULL);
}

void	ft_test_regular_input(PRINTF_PTR)
{
//	ft_test_str(p);
//	ft_test_combined(p);
//	ft_test_no_conversion(p);
//	ft_test_int(p);
//	ft_test_uint(p);
//	ft_test_uhex(p);
//	ft_test_lhex(p);
	ft_test_ptr(p);
}
